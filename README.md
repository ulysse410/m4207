TD1 : Update carte, branchement, test blink    OK

TD2 :

Ex. 0 : OK

EX. 1: Basic skills...

int x = 0;
void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.print(x);
  delay(1000);
  x = x+1;
  Serial.print("  ");
}

RESULTAT : 0  1  2  3  4  5  6  7 ...


EX. 2: GPS test

Ce qui donne : 
LGPS loop
$GPGGA,000133.000,8960.0000,N,00000.0000,E,0,0,,137.0,M,13.0,M,,*42

UTC timer  0- 1-33
latitude =  8960.0000, longitude =     0.0000
satellites number = 0



EX. 3: WIFI test Qui donne

Connecting to AP
SSID: ulysse
IP Address: 192.168.43.74
subnet mask: 255.255.255.0
gateway IP: 192.168.43.1
signal strength (RSSI):-33 dBm
Start Server
Server Started

